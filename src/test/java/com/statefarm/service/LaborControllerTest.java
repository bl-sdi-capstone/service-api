package com.statefarm.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.statefarm.service.model.User;
import com.statefarm.service.values.Seeder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class LaborControllerTest {

    @MockBean
    Seeder seeder;

    @MockBean
    SeederService seederService;

    Labor labor;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    LaborService laborService;

    ObjectMapper mapper = new ObjectMapper();
    TokenTestUtils tokenUtil = new TokenTestUtils("blah");
    String token = tokenUtil.getToken("userA", Arrays.asList("ROLE_USER"));

    @BeforeEach
    void setup() {
        labor = new Labor();
    }

    @Test
    void getAllServices() throws Exception {

        //act
        when(laborService.getLabors(eq(null), eq(null))).thenReturn(new LaborList(List.of(labor)));

        //assert
        mockMvc.perform(get("/api/services"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services", hasSize(1)));
    }

    @Test
    void getReturnsNoContent() throws Exception {
        doThrow(new LaborNotFoundException()).when(laborService).getLabors(eq(null), eq(null));

        mockMvc.perform(get("/api/services"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser
    void getAllServicesByCategory() throws Exception {

        //act
        when(laborService.getLabors(anyString(), eq(null))).thenReturn(new LaborList(List.of(labor)));

        //assert
        mockMvc.perform(get("/api/services?category=childcare"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services", hasSize(1)));
    }

    @Test
    void getAllServicesByLocation() throws Exception {

        //act
        when(laborService.getLabors(eq(null), anyString())).thenReturn(new LaborList(List.of(labor)));

        //assert
        mockMvc.perform(get("/api/services?location=Dallas"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services", hasSize(1)));
    }

    @Test
    void getAllServicesByLocationAndCategory() throws Exception {

        //act
        when(laborService.getLabors(anyString(), anyString())).thenReturn(new LaborList(List.of(labor)));

        //assert
        mockMvc.perform(get("/api/services?category=childcare&location=Dallas"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services", hasSize(1)));
    }

    @Test
    void getServiceById() throws Exception {
        labor.setId(1234L);
        when(laborService.getLaborById(anyLong())).thenReturn(labor);
        mockMvc.perform(get("/api/services/1234"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value("1234"));
    }

    @Test
    void getServiceByIdReturns204() throws Exception {
        doThrow(new LaborNotFoundException()).when(laborService).getLaborById(anyLong());

        mockMvc.perform(get("/api/services/344"))
                .andExpect(status().isNoContent());
    }

    @Test
    void getServicesByKeyWordInDescriptionAndTitle() throws Exception {
        labor.setId(123L);
        labor.setDescription("The quick brown fox jumped over the lazy dog.");
        when(laborService.getLaborsByKeyWord(anyString())).thenReturn(new LaborList(List.of(labor)));
        mockMvc.perform(get("/api/services/search?word=fox"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services[0].id").value("123"));
    }

    @Test
    void getServicesByKeyWord_Returns204() throws Exception {
        doThrow(new LaborNotFoundException()).when(laborService).getLaborsByKeyWord(anyString());
        mockMvc.perform(get("/api/services/search?word=fox"))
                .andExpect(status().isNoContent());
    }

    @Test
    void getByUserIDReturnsLabors() throws Exception {
        labor.setUsername("blahh");
        when(laborService.getLaborsByUsername(anyString())).thenReturn(new LaborList(List.of(labor)));

        mockMvc.perform(get("/api/services/user")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }


    @Test
    void postANewServiceSuccessfully() throws Exception {

        //arrange
        labor.setCategory("childcare");

        when(laborService.addLabor(any(Labor.class))).thenReturn(labor);

        mockMvc.perform(post("/api/services")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(labor)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("category").value("childcare"));

    }

    @Test
    void postReturns400() throws Exception {
        when(laborService.addLabor(any(Labor.class))).thenThrow(InvalidLaborException.class);
        String json = "{\"blah\":\"bleh\"}";
        mockMvc.perform(post("/api/services")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void patchAnExistingService() throws Exception {
        labor.setId(1234L);
        labor.setCategory("education");
        labor.setExperienceYears(3);
        labor.setDescription("Updated description text.");
        Labor update = new Labor();
        update.setCategory("education");
        update.setExperienceYears(3);
        update.setDescription("Updated description text.");
        when(laborService.updateLabor(anyLong(), any(Labor.class), any(User.class))).thenReturn(labor);
        mockMvc.perform(patch("/api/services/1234")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("category").value("education"))
                .andExpect(jsonPath("experienceYears").value("3"))
                .andExpect(jsonPath("description").value("Updated description text."));
    }

    @Test
    void patchReturns400() throws Exception {
        doThrow(new InvalidLaborException("")).when(laborService).updateLabor(anyLong(), any(Labor.class), any(User.class));
        String json = "{\"blah\":\"bleh\"}";
        mockMvc.perform(patch("/api/services/43545")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void patchReturns204() throws Exception {
        doThrow(new LaborNotFoundException()).when(laborService).updateLabor(anyLong(), any(Labor.class), any(User.class));

        Labor update = new Labor();
        update.setCategory("education");
        update.setExperienceYears(3);
        update.setDescription("Updated description text.");

        mockMvc.perform(patch("/api/services/2462")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(update)))
                .andExpect(status().isNoContent());

    }

    @Test
    void deleteAnExistingService() throws Exception {
        labor.setId(123L);

        mockMvc.perform(delete("/api/services/123")
                .header("Authorization", token))
                .andExpect(status().isAccepted());


        verify(laborService).deleteServiceByID(anyLong(), any(User.class));
    }

    @Test
    void deleteReturns204() throws Exception {
        doThrow(new LaborNotFoundException()).when(laborService).deleteServiceByID(anyLong(), any(User.class));

        mockMvc.perform(delete("/api/services/1")
                        .header("Authorization", token))
                .andExpect(status().isNoContent());
    }

    @Test
    void countsEndpointreturns200() throws Exception {
        Counts counts = new Counts();
        counts.setCategory("Athletics");
        counts.setCount(6);
        CountsList countsList = new CountsList(List.of(counts));
        when(laborService.getCounts(null)).thenReturn(countsList);

        mockMvc.perform(get("/api/services/counts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.categories", hasSize(1)));
    }

}
