package com.statefarm.service;

import com.statefarm.service.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LaborServiceTest {
    LaborService laborService;
    Labor labor;

    User user = new User(7L, "blah", "blah", "also blah", "still blah", "eaeghea");

    @Mock
    LaborRepository laborRepository;

    @BeforeEach
    void setUp() {
        laborService = new LaborService(laborRepository);
        labor = new Labor();
    }

    @Test
    void getAllServices_noParams() {
        when(laborRepository.findAllByOrderByPostedDateDesc()).thenReturn(List.of(labor));
        LaborList laborList = laborService.getLabors(null, null);
        assertThat(laborList).isNotNull();
        assertThat(laborList.isEmpty()).isFalse();
    }

    @Test
    void getAllServices_category() {
        when(laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc(anyString())).thenReturn(List.of(labor));
        LaborList laborList = laborService.getLabors("education", eq(null));
        assertThat(laborList).isNotNull();
        assertThat(laborList.isEmpty()).isFalse();
    }

    @Test
    void getAllServices_location() {
        when(laborRepository.findByLocationIgnoreCaseOrderByPostedDateDesc(anyString())).thenReturn(List.of(labor));
        LaborList laborList = laborService.getLabors(eq(null), "Dallas");
        assertThat(laborList).isNotNull();
        assertThat(laborList.isEmpty()).isFalse();
    }

    @Test
    void getAllServices_locationAndCategory() {
        when(laborRepository.findByLocationIgnoreCaseAndCategoryIgnoreCaseOrderByPostedDateDesc(anyString(), anyString())).thenReturn(List.of(labor));
        LaborList laborList = laborService.getLabors("education", "Dallas");
        assertThat(laborList).isNotNull();
        assertThat(laborList.isEmpty()).isFalse();
    }

    @Test
    void getAllServices_locationAndCategory_notFound() {
        List<Labor> emptyList = new ArrayList<>();
        when(laborRepository.findByLocationIgnoreCaseAndCategoryIgnoreCaseOrderByPostedDateDesc(anyString(), anyString()))
                .thenReturn(emptyList);
        assertThatExceptionOfType(LaborNotFoundException.class)
                .isThrownBy(() -> laborService.getLabors(anyString(), anyString()));
    }

    @Test
    void getServiceById() {
        labor.setId(1234L);
        when(laborRepository.findById(anyLong())).thenReturn(Optional.of(labor));
        Labor labor2 = laborService.getLaborById(1234L);
        assertThat(labor2.getId()).isEqualTo(labor.getId());
    }

    @Test
    void getServiceById_notFound() {
        when(laborRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatExceptionOfType(LaborNotFoundException.class)
                .isThrownBy(() -> laborService.getLaborById(anyLong()));
    }

    @Test
    void getServicesByKeyWordInDescriptionAndTitle() {
        when(laborRepository.findByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCaseOrCategoryContainingIgnoreCaseOrderByPostedDateDesc(anyString(), anyString(), anyString())).thenReturn(List.of(labor));
        LaborList laborList = laborService.getLaborsByKeyWord("fox");
        assertThat(laborList).isNotNull();
        assertThat(laborList.isEmpty()).isFalse();
    }

    @Test
    void getServicesByKeyWord_Returns204() {
        when(laborRepository.findByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCaseOrCategoryContainingIgnoreCaseOrderByPostedDateDesc(anyString(), anyString(), anyString())).thenReturn(new ArrayList<>());
        assertThatExceptionOfType(LaborNotFoundException.class)
                .isThrownBy(() -> laborService.getLaborsByKeyWord("word"));
    }

    @Test
    void getServicesByAliasSucessfully(){
        when(laborRepository.findByUsernameOrderByPostedDateDesc(anyString())).thenReturn(List.of(labor));
        LaborList list = laborService.getLaborsByUsername("blah");
        assertThat(list).isNotNull();
        assertThat(list.isEmpty()).isFalse();
    }

    @Test
    void addAService() {
        labor.setCategory("childcare");
        labor.setDescription("this is a new service");
        labor.setTitle("Test");
        labor.setLocation("Dallas");

        when(laborRepository.save(any(Labor.class))).thenReturn(labor);

        Labor labor2 = laborService.addLabor(labor);

        assertThat(labor2).isNotNull();
        assertThat(labor2.getCategory()).isEqualTo("childcare");
    }

    @Test
    void updateLaborByID() throws IllegalAccessException {
        Labor update = new Labor();
        update.setCategory("Grooming");
        update.setDescription("Please work");
        labor.setUsername(user.getUsername());
        when(laborRepository.findById(anyLong())).thenReturn(Optional.of(labor));
        when(laborRepository.save(any(Labor.class))).thenReturn(labor);
        Labor returnedLabor = laborService.updateLabor(1, update, user);
        assertThat(returnedLabor).isNotNull();
        assertThat(labor.getCategory()).isEqualTo(update.getCategory());
    }

    @Test
    void updateLaborById_laborNotFound() {
        when(laborRepository.findById(anyLong()))
                .thenReturn(Optional.empty());
        assertThatExceptionOfType(LaborNotFoundException.class)
                .isThrownBy(() -> laborService.updateLabor(1234L, any(Labor.class), user));
    }

    @Test
    void deleteService() {
        labor.setId(123L);
        labor.setUsername(user.getUsername());
        when(laborRepository.findById(anyLong()))
                .thenReturn(Optional.of(labor));
        laborService.deleteServiceByID(labor.getId(), user);
        verify(laborRepository).delete(any(Labor.class));
    }

    @Test
    void deleteService_notFound() {
        labor.setId(123L);
        when(laborRepository.findById(anyLong()))
                .thenReturn(Optional.empty());
        assertThatExceptionOfType(LaborNotFoundException.class)
                .isThrownBy(() -> laborService.deleteServiceByID(labor.getId(), user));
    }
}
