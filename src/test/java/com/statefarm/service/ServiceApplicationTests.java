package com.statefarm.service;

import com.statefarm.service.values.AllowedValues;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ServiceApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    TestRestTemplate restTemplate;
    RestTemplate patchRestTemplate;

    @Autowired
    LaborRepository laborRepository;


    String username = "UserA";
    String generateToken() {
        TokenTestUtils tokenUtil = new TokenTestUtils("blah");
        return tokenUtil.getToken(username, List.of("ROLE_USER"));
    }


    Random r = new Random();

    @BeforeEach
    void setup() {
        this.patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

        String[] titles = {"Title 1", "Title2", "Title3", "Title4", "Title5"};
        String[] locations = {"Atlanta", "Bloomington", "Dallas", "Phoenix", "Remote"};
        String[] categories = {"Childcare", "Childcare", "Athletics", null, "3asyMoney"};
        String[] descriptions = {"fuzzy living room sofa, has 3 pillows", "broken ipod, it needs repair", "silver vase", "fun fifth wheel camper", "howdy"};
        String[] usernames = {"UserA", "UserA", "UserA", "UserA", "UserZ"};

        Boolean[] activeStates = {false, true, true, true, true};


        for (int i = 0; i < 5; i++) {
            Labor labor = new Labor();
            labor.setTitle(titles[i]);
            labor.setCategory(categories[i]);
            labor.setLocation(locations[i]);
            labor.setDescription(descriptions[i]);
            labor.setUsername(usernames[i]);
            labor.setActiveStatus(activeStates[i]);
            laborRepository.save(labor);
        }
    }

    @AfterEach
    void teardown() {
        laborRepository.deleteAll();
    }

    @Test
    void getAllLabors() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
    void getAllByCategory() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services?category=Childcare", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
    void getAllByLocation() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services?location=Dallas", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
    void getAllByCategoryAndLocation() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services?category=Childcare&location=Atlanta", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().isEmpty()).isFalse();
    }

    @Test
    void getServicesByKeyWordInDescriptionTitleAndCategory() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services/search?word=3", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().size()).isEqualTo(3);
    }

    @Test
    void getServicesByKeyWordInDescriptionTitleAndCategory_IgnoresCase() {
        ResponseEntity<LaborList> response = restTemplate.getForEntity("/api/services/search?word=SiLvEr", LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().size()).isEqualTo(1);
    }

    @Test
    void getServicesByUserId() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", generateToken());
        HttpEntity<Void> request = new HttpEntity<>(null, headers);
        ResponseEntity<LaborList> response = patchRestTemplate.exchange("/api/services/user", HttpMethod.GET,request, LaborList.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().size()).isEqualTo(4);
    }

    @Test
    void addService_returnsService() {
        Labor newLabor = new Labor();
        newLabor.setDescription("this is a new service");
        newLabor.setTitle("Test");
        newLabor.setCategory("Grooming");
        newLabor.setLocation("Tx");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", generateToken());
        HttpEntity<Labor> request = new HttpEntity<>(newLabor, headers);
        ResponseEntity<Labor> response = restTemplate.postForEntity("/api/services", request, Labor.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getDescription()).isEqualTo(newLabor.getDescription());
        assertThat(response.getBody().getUsername()).isEqualTo(username);
    }
    @Test
    void addService_noLocation_returns400() {
        Labor newLabor = new Labor();
        newLabor.setDescription("this is a new service");
        newLabor.setTitle("Test");
        newLabor.setCategory("Grooming");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", generateToken());
        HttpEntity<Labor> request = new HttpEntity<>(newLabor, headers);
        ResponseEntity<ErrorMessage> response = restTemplate.postForEntity("/api/services", request, ErrorMessage.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Field 'location' cannot be blank. Provided value: null");
    }

    @Test
    void addService_noCategory_returns400() {
        Labor newLabor = new Labor();
        newLabor.setDescription("this is a new service");
        newLabor.setTitle("Test");
        newLabor.setCategory(" ");
        newLabor.setLocation("IL");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", generateToken());
        HttpEntity<Labor> request = new HttpEntity<>(newLabor, headers);
        ResponseEntity<ErrorMessage> response = restTemplate.postForEntity("/api/services", request, ErrorMessage.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Field 'category' cannot be blank. Provided value:  ");
    }

    @Test
    void addService_noDescription_returns400() {
        Labor newLabor = new Labor();
        newLabor.setTitle("test title");
        newLabor.setCategory("Childcare");
        newLabor.setLocation("GA");
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.setBearerAuth(generateToken());
        HttpEntity<Labor> request = new HttpEntity<>(newLabor, header);
        ResponseEntity<ErrorMessage> response = restTemplate.postForEntity("/api/services", request, ErrorMessage.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
        assertThat(response.getBody().getMessage()).isEqualTo("Field 'description' cannot be blank. Provided value: null");
    }

    @Test
    void addService_noTitle_returns400() {
        Labor newLabor = new Labor();
        newLabor.setDescription("test description");
        newLabor.setCategory("Childcare");
        newLabor.setLocation("TX");
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.setBearerAuth(generateToken());
        HttpEntity<Labor> request = new HttpEntity<>(newLabor, header);
        ResponseEntity<ErrorMessage> response = restTemplate.postForEntity("/api/services", request, ErrorMessage.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(400);
        assertThat(response.getBody().getMessage()).isEqualTo("Field 'title' cannot be blank. Provided value: null");
    }

    @Test
    void patchLabor_returnsUpdatedLabor() throws JSONException {
        List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
        Labor specimen = laborList.get(0);
        Long specimenId = specimen.getId();

        JSONObject updateBody = new JSONObject();
        updateBody.put("experienceYears", 3);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", generateToken());
        HttpEntity<String> request = new HttpEntity<>(updateBody.toString(), headers);

        ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/services/" + specimenId, HttpMethod.PATCH, request, Labor.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getExperienceYears()).isEqualTo(3);
        assertThat(response.getBody().getId()).isEqualTo(specimenId);
    }

    @Test
    void deleteLabor_actuallyDeletes_returns202() {
        List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
        Labor specimen = laborList.get(0);
        Long specimenId = specimen.getId();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", generateToken());
        HttpEntity<String> request = new HttpEntity<>("", headers);
        ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/services/" + specimenId, HttpMethod.DELETE, request, Labor.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(laborRepository.findById(specimenId)).isEmpty();
    }

    @Test
    void deleteLabor_notOwningUser_returns403() {
        List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
        Labor specimen = laborList.get(0);
        Long specimenId = specimen.getId();

        TokenTestUtils tokenUtil = new TokenTestUtils("blah");
        String token = tokenUtil.getToken("userB", List.of("ROLE_USER"));

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        HttpEntity<String> request = new HttpEntity<>("", headers);
        ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/services/" + specimenId, HttpMethod.DELETE, request, Labor.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(laborRepository.findById(specimenId)).isNotEmpty();
    }
        @Test
        void deleteLabor_noToken_returns401() {
            List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
            Labor specimen = laborList.get(0);
            Long specimenId = specimen.getId();

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "");
            HttpEntity<String> request = new HttpEntity<>("", headers);
            ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/services/" + specimenId, HttpMethod.DELETE, request, Labor.class);
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
            assertThat(laborRepository.findById(specimenId)).isNotEmpty();
    }

    @Test
    void updateDoesntChangeIDorPostedDateOrUserId() throws JSONException {
        List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
        Labor foundLabor = laborList.get(0);
        Long foundID = foundLabor.getId();
        Instant foundDate = foundLabor.getPostedDate();

        Long newId = foundID + 5L;
        String newUsername = "5847392L";
        Instant newDate = Instant.now().plus(10L, ChronoUnit.DAYS);

        JSONObject updateBody = new JSONObject();
        updateBody.put("id", newId);
        updateBody.put("postedDate", newDate);
        updateBody.put("username", newUsername);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", generateToken());
        HttpEntity<String> request = new HttpEntity<>(updateBody.toString(), headers);
        ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/services/" + foundID, HttpMethod.PATCH, request, Labor.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotEqualTo(newId);
        assertThat(response.getBody().getPostedDate()).isNotEqualTo(newDate);
        assertThat(response.getBody().getUsername()).isNotEqualTo(newUsername);
    }

    @Test
    void getCorrectCountsForAllStates() {
        ResponseEntity<Counts> response = restTemplate.getForEntity("/api/services/counts", Counts.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    void getsCorrectCountsForTX() {
        ResponseEntity<Counts> response = restTemplate.getForEntity("/api/services/counts?location=Dallas&category=Childcare", Counts.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
    }

   @Test
    void getCategories(){
        ResponseEntity<AllowedValues> response = restTemplate.getForEntity("/api/allowedvalues", AllowedValues.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
   }

   @Test
    void updateFlaggedStatus_returns200() {
       List<Labor> laborList = laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc("Childcare");
       Labor flaggedLabor = laborList.get(0);
       Long flaggedId = flaggedLabor.getId();
       FlagUpdate flagUpdate = new FlagUpdate(true);
       HttpHeaders header = new HttpHeaders();
       header.setContentType(MediaType.APPLICATION_JSON);
       header.setBearerAuth(generateToken());
       HttpEntity<FlagUpdate> request = new HttpEntity<>(flagUpdate, header);
       ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/flagged/" + flaggedId, HttpMethod.PATCH, request, Labor.class);
       assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
       assertThat(response.getBody().getViolationFlagged()).isTrue();
    }
    @Test
    void updateFlaggedStatus_returns204() {
        FlagUpdate flagUpdate = new FlagUpdate(true);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        header.setBearerAuth(generateToken());
        HttpEntity<FlagUpdate> request = new HttpEntity<>(flagUpdate, header);
        ResponseEntity<Labor> response = patchRestTemplate.exchange("/api/flagged/987654321", HttpMethod.PATCH, request, Labor.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(204);
    }
}
