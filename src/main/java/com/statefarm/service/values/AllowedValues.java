package com.statefarm.service.values;

import java.util.List;

public class AllowedValues {

    protected List<LaborPaymentMethod> paymentMethods;
    protected List<LaborLocation> locations;
    protected List<LaborCategory> categories;
    protected List<LaborContactMethod> contactMethods;
    protected List<LaborPriceUnit> priceUnits;

    public List<LaborPaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<LaborPaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public List<LaborLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<LaborLocation> locations) {
        this.locations = locations;
    }

    public List<LaborCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<LaborCategory> categories) {
        this.categories = categories;
    }

    public List<LaborContactMethod> getContactMethods() {
        return contactMethods;
    }

    public void setContactMethods(List<LaborContactMethod> contactMethods) {
        this.contactMethods = contactMethods;
    }

    public List<LaborPriceUnit> getPriceUnits() {
        return priceUnits;
    }

    public void setPriceUnits(List<LaborPriceUnit> priceUnits) {
        this.priceUnits = priceUnits;
    }
}
