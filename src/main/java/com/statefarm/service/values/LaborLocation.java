package com.statefarm.service.values;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LaborLocation {

   @Id
    protected String name;

    public LaborLocation(String name) {
        this.name = name;
    }

    public LaborLocation() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

