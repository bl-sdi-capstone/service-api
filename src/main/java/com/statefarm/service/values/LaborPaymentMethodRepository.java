package com.statefarm.service.values;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaborPaymentMethodRepository extends JpaRepository<LaborPaymentMethod, String> {
}
