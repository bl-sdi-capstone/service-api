package com.statefarm.service.values;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LaborPriceUnit {

    @Id
    String name;
    Integer sortOrder;

    public LaborPriceUnit(String name, Integer sortOrder) {
        this.name = name;
        this.sortOrder = sortOrder;
    }

    public LaborPriceUnit() {
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
