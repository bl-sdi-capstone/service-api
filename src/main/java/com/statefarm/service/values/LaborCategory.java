package com.statefarm.service.values;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LaborCategory {

    @Id
    protected String name;

    public LaborCategory(String name) {
        this.name = name;
    }

    public LaborCategory() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
