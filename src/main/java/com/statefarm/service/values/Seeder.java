package com.statefarm.service.values;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Seeder {

    @Autowired
    LaborCategoryRepository categoryRepository;
    @Autowired
    LaborPaymentMethodRepository paymentMethodRepository;
    @Autowired
    LaborContactMethodRepository contactMethodRepository;
    @Autowired
    LaborLocationRepository locationRepository;
    @Autowired
    LaborPriceUnitRepository priceUnitRepository;



    public void seed(){
        List<LaborCategory> categoryList = new ArrayList<>();
        categoryList.add(new LaborCategory("Athletics"));
        categoryList.add(new LaborCategory("Cleaning"));
        categoryList.add(new LaborCategory("Skills/Trade"));
        categoryList.add(new LaborCategory("Music Lessons"));
        categoryList.add(new LaborCategory("Childcare"));
        categoryList.add(new LaborCategory("Education"));
        categoryList.add(new LaborCategory("Animal Care/Grooming"));
        categoryRepository.saveAll(categoryList);

        List<LaborPaymentMethod> paymentMethodList = new ArrayList<>();
        paymentMethodList.add((new LaborPaymentMethod("Zelle")));
        paymentMethodList.add((new LaborPaymentMethod("PayPal")));
        paymentMethodList.add((new LaborPaymentMethod("Cash")));
        paymentMethodList.add((new LaborPaymentMethod("Check")));
        paymentMethodList.add((new LaborPaymentMethod("Venmo")));
        paymentMethodList.add((new LaborPaymentMethod("Google Pay/Wallet")));
        paymentMethodList.add((new LaborPaymentMethod("Apple Pay")));
        paymentMethodRepository.saveAll(paymentMethodList);

        List<LaborLocation> locationList = new ArrayList<>();
        locationList.add(new LaborLocation("Atlanta"));
        locationList.add(new LaborLocation("Bloomington"));
        locationList.add(new LaborLocation("Dallas"));
        locationList.add(new LaborLocation("Phoenix"));
        locationList.add(new LaborLocation("Remote"));
        locationRepository.saveAll(locationList);

        List<LaborContactMethod> contactMethodList = new ArrayList<>();
        contactMethodList.add(new LaborContactMethod("E-mail"));
        contactMethodList.add(new LaborContactMethod("Phone"));
        contactMethodList.add(new LaborContactMethod("Text Message"));
        contactMethodRepository.saveAll(contactMethodList);

        List<LaborPriceUnit> priceUnitList = new ArrayList<>();
        priceUnitList.add(new LaborPriceUnit("Hourly", 1));
        priceUnitList.add(new LaborPriceUnit("Daily",2));
        priceUnitList.add(new LaborPriceUnit("Weekly",3));
        priceUnitList.add(new LaborPriceUnit("Monthly",4));
        priceUnitRepository.saveAll(priceUnitList);
    }

}
