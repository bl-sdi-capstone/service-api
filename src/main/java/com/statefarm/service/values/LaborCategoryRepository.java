package com.statefarm.service.values;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaborCategoryRepository extends JpaRepository<LaborCategory, String> {
}
