package com.statefarm.service.values;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LaborContactMethod {

    @Id
    protected String name;

    public LaborContactMethod() {
    }

    public LaborContactMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
