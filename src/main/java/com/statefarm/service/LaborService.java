package com.statefarm.service;

import com.statefarm.service.model.User;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LaborService {

    LaborRepository laborRepository;

    public LaborService(LaborRepository laborRepository) {
        this.laborRepository = laborRepository;
    }

    public LaborList getLabors(String category, String location) {
        LaborList laborList;
        if (category == null && location == null) {
            laborList = new LaborList(laborRepository.findAllByOrderByPostedDateDesc());
        } else if (location == null) {
            laborList = new LaborList(laborRepository.findByCategoryIgnoreCaseOrderByPostedDateDesc(category));
        } else if (category == null) {
            laborList = new LaborList(laborRepository.findByLocationIgnoreCaseOrderByPostedDateDesc(location));
        } else {
            laborList = new LaborList(laborRepository.findByLocationIgnoreCaseAndCategoryIgnoreCaseOrderByPostedDateDesc(location, category));
        }
        if (laborList.isEmpty()) {
            throw new LaborNotFoundException();
        }
        return laborList;
    }


    public Labor addLabor(Labor labor) {
        //location
        if (labor.getLocation() == null || labor.getLocation().isBlank()) {
            throw new InvalidLaborException(
                    String.format("Field 'location' cannot be blank. Provided value: %s", labor.getLocation()));

        }
        //category
        if (labor.getCategory() == null || labor.getCategory().isBlank()){
            throw new InvalidLaborException(
                    String.format("Field 'category' cannot be blank. Provided value: %s", labor.getCategory()));
        }
        //title
        if (labor.getTitle() == null || labor.getTitle().isBlank()){
            throw new InvalidLaborException(
                    String.format("Field 'title' cannot be blank. Provided value: %s", labor.getTitle())
            );
        }
        //description
        if (labor.getDescription() == null || labor.getDescription().isBlank()){
            throw new InvalidLaborException(
                    String.format("Field 'description' cannot be blank. Provided value: %s", labor.getDescription())
            );
        }

        LocalDate expirydate = LocalDate.now().plusYears(1);
        labor.setExpirationDate(expirydate);
        return laborRepository.save(labor);
    }

    public Labor getLaborById(long id) {
        Optional<Labor> oLabor = laborRepository.findById(id);
        if (oLabor.isPresent()) {
            return oLabor.get();
        }
        throw new LaborNotFoundException();
    }

    public Labor updateLabor(long id, Labor update, User user) throws IllegalAccessException {
        Optional<Labor> oLabor = laborRepository.findById(id);


        if (oLabor.isPresent()) {
            Labor foundLabor = oLabor.get();

            if (!foundLabor.getUsername().equals(user.getUsername())) {
                throw new UserAccessDeniedException();
            }

            for (Field field : update.getClass().getDeclaredFields()) {
                if (field.isSynthetic()) {
                    continue;
                }
                if (field.getName().equals("id") || (field.getName().equals("postedDate")) || (field.getName().equals("username"))) {
                    continue;
                }
                if (field.get(update) != null) {
                    field.set(foundLabor, field.get(update));
                }
            }
            return laborRepository.save(foundLabor);
        }
        throw new LaborNotFoundException();
    }

    public void deleteServiceByID(long id, User user) {
        Optional<Labor> oLabor = laborRepository.findById(id);

        if (oLabor.isEmpty()) {
            throw new LaborNotFoundException();
        }

        Labor foundLabor = oLabor.get();
        if (!foundLabor.getUsername().equals(user.getUsername())) {
            throw new UserAccessDeniedException();
        }

        laborRepository.delete(foundLabor);
    }

    public CountsList getCounts(String location) {

        CountsList list = new CountsList();
        List<Tuple> catTuples;
        if (location == null) {
            catTuples = laborRepository.countAllCategories();
        } else {
            catTuples = laborRepository.countCategoriesByState(location);
        }
        if (catTuples.isEmpty()) {
            throw new LaborNotFoundException();
        }
        List<Counts> countsList = new ArrayList<>();

        for (Tuple tuple : catTuples) {
            if (tuple.get(0) == null) {
                continue;
            }
            Counts counts = new Counts();
            counts.setCategory(tuple.get(0).toString());
            counts.setCount(Integer.parseInt(tuple.get(1).toString()));
            countsList.add(counts);
        }
        list.setCategories(countsList);

        if (countsList.isEmpty()) {
            throw new LaborNotFoundException();
        }
        return list;
    }

    public LaborList getLaborsByKeyWord(String word) {
        LaborList laborList = new LaborList(laborRepository.findByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCaseOrCategoryContainingIgnoreCaseOrderByPostedDateDesc(word, word, word));
        if (laborList.isEmpty()) {
            throw new LaborNotFoundException();
        }
        return laborList;
    }

    public LaborList getLaborsByUsername(String username) {
        LaborList list = new LaborList(laborRepository.findByUsernameOrderByPostedDateDesc(username));
        if (list.isEmpty()) {
            throw new LaborNotFoundException();
        }
        return list;

    }

    //TODO
    //WRITE TEST FOR UPDATING: 200 and 204
    public Labor updateFlag(long id, FlagUpdate update) {
        Optional<Labor> optionalLabor = laborRepository.findById(id);

        if (optionalLabor.isEmpty()){
            throw new LaborNotFoundException();
        }
        Labor foundLabor = optionalLabor.get();
        foundLabor.setViolationFlagged(update.getViolationFlagged());
        return laborRepository.save(foundLabor);
    }
}

