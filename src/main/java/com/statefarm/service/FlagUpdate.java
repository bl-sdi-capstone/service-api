package com.statefarm.service;

public class FlagUpdate {
    protected Boolean violationFlagged;

    public FlagUpdate(Boolean violationFlagged) {
        this.violationFlagged = violationFlagged;
    }

    public FlagUpdate() {
    }

    public Boolean getViolationFlagged() {
        return violationFlagged;
    }

    public void setViolationFlagged(Boolean violationFlagged) {
        this.violationFlagged = violationFlagged;
    }
}
