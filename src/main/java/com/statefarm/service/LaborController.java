package com.statefarm.service;

import com.statefarm.service.model.User;
import com.statefarm.service.values.AllowedValues;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class LaborController {
    public LaborService laborService;
    public SeederService seederService;

    public LaborController(LaborService laborService, SeederService seederService) {
        this.laborService = laborService;
        this.seederService = seederService;
    }

    @GetMapping("/api/services")
    public LaborList getLabors(@RequestParam(required = false) String category, @RequestParam(required = false) String location) {
        return laborService.getLabors(category, location);
    }

    @GetMapping("/api/services/{id}")
    public Labor getLabor(@PathVariable Long id) {
        return laborService.getLaborById(id);
    }

    @GetMapping("/api/services/counts")
    public CountsList getCounts(@RequestParam(required = false) String location) {
        return laborService.getCounts(location);
    }

    @GetMapping("/api/services/search")
    public LaborList getLaborsByKeyWord(@RequestParam String word) {
        return laborService.getLaborsByKeyWord(word);
    }

    @GetMapping("/api/services/user")
    public LaborList getLaborsByUserAlias(@AuthenticationPrincipal User user){
        String username = user.getUsername();
        return laborService.getLaborsByUsername(username);
    }

    @GetMapping("/api/allowedvalues")
    public AllowedValues getAllowedValues(){
        return seederService.seedAllowedValues();
    }

    @PostMapping("/api/services")
    public Labor addLabor(@RequestBody Labor labor, @AuthenticationPrincipal User user) {
        String username = user.getUsername();
        labor.setUsername(username);
        return laborService.addLabor(labor);
    }

    @PatchMapping("/api/services/{id}")
    public Labor updateLabor(@PathVariable long id,
                             @RequestBody Labor update, @AuthenticationPrincipal User user) throws IllegalAccessException {
        return laborService.updateLabor(id, update, user);
    }

    @PatchMapping("/api/flagged/{id}")
    public Labor toggleFlaggedStatus(@PathVariable long id, @RequestBody FlagUpdate update){
        return laborService.updateFlag(id, update);
    }

    @DeleteMapping("/api/services/{id}")
    public ResponseEntity<Void> deleteService(@PathVariable long id, @AuthenticationPrincipal User user) {
        laborService.deleteServiceByID(id, user);
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void laborNotFoundHandler(LaborNotFoundException e) {
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage invalidLaborHandler(InvalidLaborException e) {
        return new ErrorMessage(e.getMessage());

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void catchIllegalAccess(IllegalAccessException e) {

    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ErrorMessage expiredTokenHandler(ExpiredTokenException e){
        return new ErrorMessage(e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorMessage userAccessDeniedHandler(UserAccessDeniedException e) {
        return new ErrorMessage(e.getMessage());
    }

}
