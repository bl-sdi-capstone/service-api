package com.statefarm.service;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class CountsList {

    protected List<Counts> categories;

    public CountsList() {
        categories = new ArrayList<>();
    }

    public CountsList(List<Counts> categories) {
        this.categories = categories;
    }

    public List<Counts> getCategories() {
        return categories;
    }

    public void setCategories(List<Counts> categories) {
        this.categories = categories;
    }

    @JsonIgnore
    public Boolean isEmpty() {
        return this.categories.isEmpty();
    }

    @Override
    @JsonIgnore
    public String toString() {
        return "CountsList{" +
                "categories=" + categories +
                '}';
    }
}
