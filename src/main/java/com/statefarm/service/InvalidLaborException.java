package com.statefarm.service;

public class InvalidLaborException extends RuntimeException {

    public InvalidLaborException(String message) {
        super(message);
    }
}
