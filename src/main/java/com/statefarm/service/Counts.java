package com.statefarm.service;

public class Counts {

    protected String category;
    protected int count;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Counts{" +
                "category='" + category + '\'' +
                ", count=" + count +
                '}';
    }

}
