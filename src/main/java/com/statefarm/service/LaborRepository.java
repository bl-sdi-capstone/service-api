package com.statefarm.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public interface LaborRepository extends JpaRepository<Labor, Long> {
    List<Labor> findAllByOrderByPostedDateDesc();

    List<Labor> findByCategoryIgnoreCaseOrderByPostedDateDesc(@Param("category") String category);

    List<Labor> findByLocationIgnoreCaseOrderByPostedDateDesc(@Param("location") String location);

    List<Labor> findByLocationIgnoreCaseAndCategoryIgnoreCaseOrderByPostedDateDesc(@Param("location") String location, @Param("category") String category);

    @Query(value="SELECT category AS category, count(*) AS count FROM services s WHERE lower(s.location) = lower(?1) AND s.active_status = true GROUP BY s.category", nativeQuery = true)
    List<Tuple> countCategoriesByState(@Param("location") String location);

    @Query(value="SELECT category AS category, count(*) AS count FROM services s WHERE s.active_status = true GROUP BY s.category", nativeQuery = true)
    List<Tuple> countAllCategories();

    List<Labor> findByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCaseOrCategoryContainingIgnoreCaseOrderByPostedDateDesc(@Param("word1") String word1, @Param("word2") String word2, @Param("word3") String word3);

    List<Labor> findByUsernameOrderByPostedDateDesc(String username);

}
