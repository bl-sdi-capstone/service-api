package com.statefarm.service;
import com.statefarm.service.security.config.JwtProperties;
import com.statefarm.service.values.Seeder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ServiceApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }

    protected final Seeder seeder;

    public ServiceApplication(Seeder seeder){
        this.seeder = seeder;
    }

    @Override
    public void run(String... args){
        seeder.seed();
    }

    @Bean
    public JwtProperties getJwtProperties(){
        return new JwtProperties();
    }


}
