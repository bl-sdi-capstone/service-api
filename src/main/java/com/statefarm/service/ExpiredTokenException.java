package com.statefarm.service;

public class ExpiredTokenException extends RuntimeException {
    public ExpiredTokenException(String message) {super(message);}
}
