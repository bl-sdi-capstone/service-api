package com.statefarm.service;

import com.statefarm.service.values.*;
import org.springframework.stereotype.Service;

@Service
public class SeederService {

    LaborLocationRepository locationRepository;
    LaborCategoryRepository categoryRepository;
    LaborContactMethodRepository contactMethodRepository;
    LaborPaymentMethodRepository paymentMethodRepository;
    LaborPriceUnitRepository priceUnitRepository;

    public SeederService(LaborLocationRepository locationRepository, LaborCategoryRepository categoryRepository, LaborContactMethodRepository contactMethodRepository, LaborPaymentMethodRepository paymentMethodRepository, LaborPriceUnitRepository priceUnitRepository) {
        this.locationRepository = locationRepository;
        this.categoryRepository = categoryRepository;
        this.contactMethodRepository = contactMethodRepository;
        this.paymentMethodRepository = paymentMethodRepository;
        this.priceUnitRepository = priceUnitRepository;
    }

    public AllowedValues seedAllowedValues() {
        AllowedValues values = new AllowedValues();
        values.setCategories(categoryRepository.findAll());
        values.setLocations(locationRepository.findAll());
        values.setPaymentMethods(paymentMethodRepository.findAll());
        values.setContactMethods(contactMethodRepository.findAll());
        values.setPriceUnits(priceUnitRepository.findAll());
        return values;
    }

}
