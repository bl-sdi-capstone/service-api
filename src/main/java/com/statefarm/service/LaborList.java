package com.statefarm.service;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class LaborList {
    private List<Labor> services;

    public LaborList() {
        services = new ArrayList<>();
    }

    public LaborList(List<Labor> servicesList) {
        services = servicesList;
    }

    public List<Labor> getServices() {
        return services;
    }

    public void setServices(List<Labor> services) {
        this.services = services;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this.services.isEmpty();
    }

    @JsonIgnore
    public Integer size() {
        return this.services.size();
    }
}
