package com.statefarm.service.security.config;

import com.statefarm.service.security.filter.JwtTokenAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Configuration
@EnableWebSecurity 	// Enable security config. This annotation denotes config for spring security.
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityCredentialsConfig extends WebSecurityConfigurerAdapter {

    private JwtProperties jwtProperties;

    public SecurityCredentialsConfig( JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.cors();
        http
                .cors().and()
                .csrf().disable()
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // handle an authorized attempts
                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                .addFilterBefore(new JwtTokenAuthenticationFilter(jwtProperties), UsernamePasswordAuthenticationFilter.class)
                // Add a filter to validate user credentials and add token in the response header
                // What's the authenticationManager()?
                // An object provided by WebSecurityConfigurerAdapter, used to authenticate the user passing user's credentials
                // The filter needs this auth manager to authenticate the user.
                .authorizeRequests()
                 // SERVICES
                .antMatchers(HttpMethod.GET, "/api/services").permitAll()
                .antMatchers(HttpMethod.GET, "/api/services/{id}").permitAll()
                .antMatchers(HttpMethod.POST, "/api/services").hasRole("USER")
                .antMatchers(HttpMethod.PATCH, "/api/services/{id}").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/api/services/{id}").hasRole("USER")
                //  - COUNTS
                .antMatchers(HttpMethod.GET, "/api/services/counts").permitAll()
                //  - SEARCH
                .antMatchers(HttpMethod.GET, "/api/services/search").permitAll()
                //  - USER
                .antMatchers(HttpMethod.GET, "/api/services/user").hasRole("USER")
                // ALLOWEDVALUES
                .antMatchers(HttpMethod.GET, "/api/allowedvalues").permitAll()
                .antMatchers(HttpMethod.PATCH, "/api/flagged/{id}").permitAll()
                .anyRequest().authenticated();




                // HEALTH
//                .antMatchers(HttpMethod.GET, "/actuator/health").permitAll()
                // LOGIN
//                .antMatchers(HttpMethod.POST, jwtProperties.getUri()).permitAll()
                // REGISTER
//                .antMatchers(HttpMethod.POST, "/api/account/register/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/api/users/**").hasRole("USER")
//                .antMatchers(HttpMethod.GET, "/api/user/**").permitAll()
                // CHANGE PASSWORD
//                .antMatchers(HttpMethod.PUT, "/password/**").hasRole("USER")
                // ADMIN ACTUATOR ENDPOINTS (ALL ARE EXPOSED)
//                .antMatchers(HttpMethod.GET,"/actuator/**", "/api/admin/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/api/admin/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/api/admin/**").hasRole("ADMIN")
                // any other requests must be authenticated
//                .anyRequest().authenticated();
    }

    // Spring has UserDetailsService interface, which can be overriden to provide our implementation for fetching user from database (or any other source).
    // The UserDetailsService object is used by the auth manager to load the user from database.
    // In addition, we need to define the password encoder also. So, auth manager can compare and verify passwords.

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /*
     * Basic CORS configuration. This is a bean so it can managed by Spring and injected where needed.
     * Note: There are many ways to configure CORS in your security configuration.
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOriginPattern("*");
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setExposedHeaders(Arrays.asList("*","Authorization"));
        configuration.setAllowCredentials(true);
        configuration.applyPermitDefaultValues();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
